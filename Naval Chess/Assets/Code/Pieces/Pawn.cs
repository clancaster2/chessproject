﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pawn : Piece
{
    private bool firstTurn;
    private int moveAmount;

    // Start is called before the first frame update
    void Start()
    {
        firstTurn = true;
        moveAmount = 2;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void SelectPiece()
    {
        Debug.Log("Pawn selection");
        for (int i = 0; i < grid.tiles.Length; i++)
        {
            //Filters spaces occupied by friendly pieces for activation
            if (grid.tiles[i].isOccupied == false || grid.tiles[i].occupiedPiece.myTeam != myTeam)
            {
                if (myTeam == GameManager.Teams.black)
                {
                    //Checks for spaces within the pieces movement range
                    if (grid.tiles[i].tilePosition.x - currentTile.tilePosition.x <= moveAmount && currentTile.tilePosition.x < grid.tiles[i].tilePosition.x && grid.tiles[i].tilePosition.y == currentTile.tilePosition.y && grid.tiles[i].tilePosition.z == currentTile.tilePosition.z)
                    {
                        grid.tiles[i].Activate();
                        if (grid.tiles[i].isOccupied)
                        {
                            Debug.Log("Space Occupied");
                            //if an occupied space is found, deactivates any tiles past it that could be otherwise moved to
                            //HACK - Really shouldn't loop this many times, need to figure out a way to activate and deactivate tiles better
                            for (int x = 0; x < grid.tiles.Length; x++)
                            {
                                if (grid.tiles[x].tilePosition.x > grid.tiles[i].tilePosition.x && grid.tiles[x].isActive)
                                {
                                    grid.tiles[x].Deactivate();
                                }
                            }
                            //return;
                        }
                    }
                }
                else if (myTeam == GameManager.Teams.white)
                {
                    if (currentTile.tilePosition.x - grid.tiles[i].tilePosition.x <= moveAmount && currentTile.tilePosition.x > grid.tiles[i].tilePosition.x && grid.tiles[i].tilePosition.y == currentTile.tilePosition.y && grid.tiles[i].tilePosition.z == currentTile.tilePosition.z)
                    {
                        grid.tiles[i].Activate();
                        if (grid.tiles[i].isOccupied)
                        {
                            Debug.Log("Space Occupied");
                            //if an occupied space is found, deactivates any tiles past it that could be otherwise moved to
                            //HACK - Really shouldn't loop this many times, need to figure out a way to activate and deactivate tiles better
                            for (int x = 0; x < grid.tiles.Length; x++)
                            {
                                if (grid.tiles[x].tilePosition.x < grid.tiles[i].tilePosition.x && grid.tiles[x].isActive)
                                {
                                    grid.tiles[x].Deactivate();
                                }
                            }
                            //return;
                        }
                    }
                }
            }
        }
    }

    //Sets first turn bool to false, so that pawns can only move two spaces on their first turn
    public override void MovePiece(Tile targetTile)
    {
        base.MovePiece(targetTile);
        if (firstTurn == true)
        {
            firstTurn = false;
            moveAmount = 1;
        }
    }
}
