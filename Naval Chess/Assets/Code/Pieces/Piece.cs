﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piece : MonoBehaviour
{
    public Tile currentTile;

    [HideInInspector]
    public Grid grid;

    // public enum Teams { white, black};

    public GameManager.Teams myTeam;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Selects the piece
    public virtual void SelectPiece()
    {
        Debug.Log("Base Selection");
        //Activates all the tiles within this pieces movement/targeting space
        for (int i = 0; i < grid.tiles.Length; i++)
        {
            //Filters spaces occupied by friendly pieces for activation
            if (grid.tiles[i].isOccupied == false || grid.tiles[i].occupiedPiece.myTeam != myTeam)
            {
                grid.tiles[i].Activate();
            }
        }
    }
    
    //Deselects the piece
    public void DeselectPiece()
    {
        //Deactivates all tiles
        for (int i = 0; i < grid.tiles.Length; i++)
        {
            if (grid.tiles[i].isActive)
            {
                grid.tiles[i].Deactivate();
            }
        }
    }

    //Destroys the piece on the targeted tile before moving
    public void TakePiece(Tile targetTile)
    {
        GameObject toKill = targetTile.occupiedPiece.gameObject;
        targetTile.occupiedPiece = null;
        targetTile.isOccupied = false;
        Destroy(toKill);
        MovePiece(targetTile);
    }

    //Moves this piece to its targeted tile
    public virtual void MovePiece(Tile targetTile)
    {
        //Ensures all tile variables are correctly set when moving
        currentTile.isOccupied = false;
        currentTile.occupiedPiece = null;
        currentTile = targetTile;
        currentTile.isOccupied = true;
        currentTile.occupiedPiece = this;
        //Moves this piece slightly above the selected tile
        Vector3 newPos = new Vector3(
            targetTile.transform.position.x,
            targetTile.transform.position.y + grid.pieceHeight,
            targetTile.transform.position.z);
        transform.position = newPos;
    }
}
