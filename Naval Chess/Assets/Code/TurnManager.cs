﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : MonoBehaviour
{
    public GameManager.Teams currentTeam;

    // Start is called before the first frame update
    void Start()
    {
        //Starts the game on whites turn, as per classic chess rules
        currentTeam = GameManager.Teams.white;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public GameManager.Teams ChangeTeam()
    {
        //Toggles the current team
        if (currentTeam == GameManager.Teams.black)
        {
            currentTeam = GameManager.Teams.white;
        }      
        else
        {
            currentTeam = GameManager.Teams.black;
        }
        return currentTeam;
    }
}
