﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public Vector3 tilePosition;

    public bool isOccupied;
    public Piece occupiedPiece;

    public bool isActive;

    public MeshRenderer meshRenderer;

    public Material originalMat, moveMat, killMat;

    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Activate()
    {
        //Changes material based on the tiles occupied status when activated
        if (isOccupied)
        {
            meshRenderer.material = killMat;
        }
        else
        {
            meshRenderer.material = moveMat;
        }
        isActive = true;
    }

    //Returns the tile to its inactive state
    public void Deactivate()
    {
        meshRenderer.material = originalMat;
        isActive = false;
    }
}
