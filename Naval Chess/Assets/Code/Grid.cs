﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    [SerializeField]
    private GameObject manager;

    private PieceManager pieceManager;

    [SerializeField]
    private int gridX, gridZ, layers;

    [SerializeField]
    private float spaceScale = 1f;
    public float pieceHeight = .5f;

    [SerializeField]
    private float layerGap;

    [HideInInspector]
    public Tile[] tiles;

    [SerializeField]
    private GameObject spacePrefab;
        
    [SerializeField]
    private GameObject basePiecePrefab, pawnPrefab;

    [SerializeField]
    private Material blackSpaceMat, whiteSpaceMat, blackPieceMat, whitePieceMat, moveMat, killMat;

    public GameObject grid;

    // Start is called before the first frame update
    void Start()
    {
        if (manager.GetComponent<PieceManager>() != null)
        {
            pieceManager = manager.GetComponent<PieceManager>();
        }
        //Creates a new array, equal to the size of the grid
        tiles = new Tile[gridX * layers * gridZ];

        //Loops through the x, y(layers) and z grid coordinates. Spawns cubes to represent the grid
        //and stores references to each space in an array. Also colors the cubes based on their ID
        //in order to recreate the grid of a chess board.

        //TODO - Implement better assignments to reduce getcomponent calls
        for (int y = 0, i = 0; y < layers; y++)
        {
            for (int z = 0; z < gridZ; z++)
            {
                for (int x = 0; x < gridX; x++, i++)
                {
                    GameObject space;
                    space = Instantiate(spacePrefab, new Vector3(x * spaceScale + (y * (gridX * spaceScale)), y * layerGap, z * spaceScale), Quaternion.identity, gameObject.transform);
                    space.GetComponent<Tile>().tilePosition = new Vector3(x, y, z);
                    if (z % 2 == 0)
                    {
                        if (i % 2 == 0)
                        {
                            space.GetComponent<MeshRenderer>().material = blackSpaceMat;
                            space.GetComponent<Tile>().originalMat = blackSpaceMat;
                        }
                        else
                        {
                            space.GetComponent<MeshRenderer>().material = whiteSpaceMat;
                            space.GetComponent<Tile>().originalMat = whiteSpaceMat;
                        }
                    }
                    else
                    {
                        if (i % 2 != 0)
                        {
                            space.GetComponent<MeshRenderer>().material = blackSpaceMat;
                            space.GetComponent<Tile>().originalMat = blackSpaceMat;
                        }
                        else
                        {
                            space.GetComponent<MeshRenderer>().material = whiteSpaceMat;
                            space.GetComponent<Tile>().originalMat = whiteSpaceMat;
                        }
                    }
                    space.GetComponent<Tile>().moveMat = moveMat;
                    space.GetComponent<Tile>().killMat = killMat;
                    tiles[i] = space.GetComponent<Tile>();
                    //TODO - When individual pieces are implemented, this will need to be replaced with 
                    //Spawns a Black Piece
                    //If tiles are on the first or last two columns of each layer, adds a piece, determining that pieces team based on which side it is on
                    GameObject piece;
                    if (x < 2)
                    {
                        piece = Instantiate(pawnPrefab, space.transform.position + new Vector3(0, pieceHeight, 0), Quaternion.identity, gameObject.transform);
                        piece.GetComponent<MeshRenderer>().material = blackPieceMat;
                        piece.GetComponent<Piece>().myTeam = GameManager.Teams.black;
                    }
                    //Spawns a White Piece
                    else if (x < gridX && x >= gridX - 2)
                    {
                        piece = Instantiate(pawnPrefab, space.transform.position + new Vector3(0, pieceHeight, 0), Quaternion.identity, gameObject.transform);
                        piece.GetComponent<MeshRenderer>().material = whitePieceMat;
                        piece.GetComponent<Piece>().myTeam = GameManager.Teams.white;
                    }
                    else { piece = null; }
                    if (piece != null)
                    {
                        //Sets up both piece and tile variables as necessary
                        piece.GetComponent<Piece>().currentTile = space.GetComponent<Tile>();
                        space.GetComponent<Tile>().isOccupied = true;
                        space.GetComponent<Tile>().occupiedPiece = piece.GetComponent<Piece>();
                        piece.GetComponent<Piece>().grid = this;
                        //HACK - This should be replaced by an event call, that is primarily listened to
                        //by the piece manager, but is there if it is needed later
                        pieceManager.pieces.Add(piece);
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
