﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    TurnManager turnManager;
    private GameManager.Teams currentTeam;
    Piece selectedPiece;
    Tile selectedTile;

    // Start is called before the first frame update
    void Start()
    {
        //HACK - Stores the turn manager and starting team
        if (GameObject.Find("Manager").GetComponent<TurnManager>() != null)
        {
            turnManager = GameObject.Find("Manager").GetComponent<TurnManager>();
            currentTeam = turnManager.currentTeam;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Left mouse input
        if (Input.GetMouseButtonDown(0))
        {
            //Creates a raycast from mouse position into screen
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1000))
            {
                //Debug.DrawRay(ray.origin, ray.direction, Color.red, .5f);
                //Raycast against piece
                if (hit.collider.GetComponent<Piece>() != null)
                {
                    //Deselects tile if a piece is selected
                    if (selectedTile != null)
                    {
                        selectedTile = null;
                    }
                    //Check to see if an enemy piece has been selected and if its space is active
                    if (selectedPiece != null && hit.collider.GetComponent<Piece>().myTeam != turnManager.currentTeam && hit.collider.GetComponent<Piece>().currentTile.isActive == true)
                    {
                        //If the player selects an enemy piece, targets the tile it is on
                        selectedTile = hit.collider.GetComponent<Piece>().currentTile;
                    }
                    //Deselects current piece if new piece is selected
                    else if (selectedPiece != null && selectedPiece != hit.collider.GetComponent<Piece>())
                    {
                        selectedPiece.DeselectPiece();
                    }
                    //Stores reference to selected piece and runs selection function
                    if (hit.collider.GetComponent<Piece>().myTeam == turnManager.currentTeam)
                    {
                        selectedPiece = hit.collider.GetComponent<Piece>();
                        selectedPiece.SelectPiece();
                    }
                }
                //Checks if the player currently has a piece selected
                else if (selectedPiece != null)
                {
                    //Selects a tile to target, storing a reference to it 
                    if (hit.collider.GetComponent<Tile>() != null && hit.collider.GetComponent<Tile>().isActive)
                    {
                        selectedTile = hit.collider.GetComponent<Tile>();
                    }
                    else
                    {
                        //Deselects everything, as the player has clicked on either empty space, or an unselectable object
                        if (selectedTile != null)
                        {
                            selectedTile = null;
                        }
                        if (selectedPiece != null)
                        {
                            selectedPiece.DeselectPiece();
                            selectedPiece = null;
                        }
                    }
                }
            }
            else
            {
                //Deselects everything, as the player has clicked on either empty space, or an unselectable object
                if (selectedTile != null)
                {
                    selectedTile = null;
                }
                if (selectedPiece != null)
                {
                    selectedPiece.DeselectPiece();
                    selectedPiece = null;
                }
            }
        }
        //C Input (For Confirmation) TOREPLACE - C input will later be replaced with a UI button input
        if (Input.GetKeyDown(KeyCode.C))
        {
            //Moves the selected piece to the targeted tile, taking the enemy piece if the target tile is occupied.
            if (selectedPiece && selectedTile != null)
            {
                if (selectedTile.isOccupied == true)
                {
                    selectedPiece.TakePiece(selectedTile);
                }
                else
                {
                    selectedPiece.MovePiece(selectedTile);
                }
                //Deselects everything
                selectedTile = null;
                selectedPiece.DeselectPiece();
                selectedPiece = null;
            }
            FinishTurn();
        }
    }

    void FinishTurn()
    {
        //Switches teams for next turn
        currentTeam = turnManager.ChangeTeam();
    }
}
